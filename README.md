# Schachnovelle
_»Um Gottes Willen! Nicht!«_

[![Find the code on GitLab](https://upload.wikimedia.org/wikipedia/commons/d/d9/Chess_clt45.svg)](https://gitlab.com/manegame/schachnovelle)


_[manusnijhoff.nl](https://manusnijhoff.nl)_,
_[Read the book](https://en.wikipedia.org/wiki/The_Royal_Game)_, _[Play me on lichess](https://lichess.org/@/manegame)_,


## Installation
Use yarn

`yarn global add schachnovelle`

or npm

`npm i -g schachnovelle`

## Launch
`schachnovelle`

...And play!

## Features
You can play against yourself or a friend on the same keyboard.

## Roadmap

I'd love to connect this to the [lichess](https://lichess.org/) API, so you can play others from the comfort of the command line!

## Changelog

_[Changelog](CHANGELOG.md)_