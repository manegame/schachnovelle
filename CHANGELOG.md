# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project (tries to) adhere to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
<!-- ### Added
### Changed
### Removed -->

## [1.0.1] - 2020-12-16
### Added
- Changelog

## [1.0.0] - 2020-12-16
### Added
- Promotion to different pieces

### Changed
- Clear the console instead of adding to the end of the line

### Removed