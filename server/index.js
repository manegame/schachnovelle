require('dotenv').config()

// 
// 
// Imports
const express = require('express')
// Storage
const open = require('open')
const session = require('express-session')
const MemoryStore = require('memorystore')(session)
// Lichess
const lichess = require('./api')

/* Create your lichess OAuth app on https://lichess.org/account/oauth/app/create
 * Homepage URL: http://localhost:3000
 * Callback URL: http://localhost:3000/callback
 */



//
//
// Server + Auth config
const port = 3000
const host = process.env.NODE_ENV === 'production' ? 'https://schachnovelle.online' : 'http://localhost'
const clientId = process.env.LICHESS_ID
const clientSecret = process.env.LICHESS_SECRET
const redirectUri = `http://localhost:${port}/callback`

// Scopes
const scopes = [
  'preference:read',
  'challenge:read',
  'board:play',
  'bot:play'
];



//
//
// Lichess
const tokenHost = 'https://oauth.lichess.org/';
const tokenPath = '/oauth';
const authorizePath = '/oauth/authorize';

const oAuthConfig = {
  client: {
    id: clientId,
    secret: clientSecret
  },
  auth: {
    tokenHost,
    tokenPath,
    authorizePath
  }
}

const { AuthorizationCode } = require('simple-oauth2')
const client = new AuthorizationCode(oAuthConfig)



//
//
// Initialize the server
const app = express();



// 
// 
// Set things
app.set('view engine', 'pug')



//
//
// Session
app.use(session({
  cookie: { maxAge: 86400000 },
  store: new MemoryStore({
    checkPeriod: 86400000 // prune expired entries every 24h
  }),
  resave: false,
  secret: 'keyboard cat',
  saveUninitialized: true
}))



//
//
// Show the "log in with lichess" button
app.get('/', (req, res) => {
  if (req.session.userInfo) {
    res.redirect('/welcome')
  } else {
    res.render('../views/index')
  }
})



//
//
// Initial page redirecting to Lichess
app.get('/auth', (req, res) => {
  const state = Math.random().toString(36).substring(2)

  const authorizationUri = client.authorizeURL({
    redirect_uri: redirectUri,
    scope: scopes.join(' '),
    state
  })

  res.redirect(authorizationUri)
})


//
//
// callback page
app.get('/callback', async (req, res) => {
  const tokenParams = {
    code: req.query.code,
    redirect_uri: redirectUri,
    scope: scopes.join(' ')
  }

  try {
    const result = await client.getToken(tokenParams)
    const accessToken = client.createToken(result)

    // Save token to the session
    req.session.accessToken = accessToken.token.token

    res.redirect('/welcome')
  } catch (error) {
    console.log('Access Token Error', error.message)
  }
})



//
//
// Welcome page
app.get('/welcome', async (req, res) => {
  if (req.session.accessToken) {    
    try {
      const userInfo = await lichess.getUserInfo(req.session.accessToken)
      req.session.userInfo = userInfo.data
    } catch (error) {
      console.error(error)
    } finally {
      if (req.session.userInfo) {
        lichess.startPlay(req, res)
          .then((response) => {
            if (response.status === 200) {
              // This goes to the UI
              process.stdout.write('streamReady')
              // This to the browser
              res.render('../views/welcome', { user: req.session.userInfo })
            } else {
              res.render('../views/error', response)
            }
          })
          .catch((error) => {
            console.error(error)
            res.render('../views/error', error)
          })
        } else {
        res.render('../views/error')
      }
    }
    
  } else {
    res.redirect('/')
  }
})

app.listen(port, async () => {
  await open(`${host}:${port}`)
});



// SAMPLE DATA

// { type: 'gameStart', game: { id: 'Wmv3QOtD' } }
// {
//   id: 'Wmv3QOtD',
//   variant: { key: 'standard', name: 'Standard', short: 'Std' },
//   clock: { initial: 600000, increment: 3000 },
//   speed: 'rapid',
//   perf: { name: 'Rapid' },
//   rated: false,
//   createdAt: 1612916752568,
//   white: { id: 'manegame', name: 'manegame', title: null, rating: 1155 },
//   black: {
//     id: 'schachnovelle-online',
//     name: 'schachnovelle-online',
//     title: null,
//     rating: 1500,
//     provisional: true
//   },
//   initialFen: 'startpos',
//   type: 'gameFull',
//   state: {
//     type: 'gameState',
//     moves: '',
//     wtime: 600000,
//     btime: 600000,
//     winc: 3000,
//     binc: 3000,
//     wdraw: false,
//     bdraw: false,
//     status: 'started'
//   }
// }
// {
//   type: 'gameState',
//   moves: 'e2e4',
//   wtime: 600000,
//   btime: 600000,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5',
//   wtime: 600000,
//   btime: 600000,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3',
//   wtime: 599590,
//   btime: 600000,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6',
//   wtime: 599590,
//   btime: 597200,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6 f1c4',
//   wtime: 598580,
//   btime: 597200,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6 f1c4 d7d5',
//   wtime: 598580,
//   btime: 595560,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6 f1c4 d7d5 c4d5',
//   wtime: 592540,
//   btime: 595560,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6 f1c4 d7d5 c4d5 c6d5',
//   wtime: 592540,
//   btime: 595370,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6 f1c4 d7d5 c4d5 c6d5 f3e5',
//   wtime: 591460,
//   btime: 595370,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6 f1c4 d7d5 c4d5 c6d5 f3e5 d5e4',
//   wtime: 591460,
//   btime: 594720,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6 f1c4 d7d5 c4d5 c6d5 f3e5 d5e4 g2g3',
//   wtime: 578720,
//   btime: 594720,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6 f1c4 d7d5 c4d5 c6d5 f3e5 d5e4 g2g3 d8d2',
//   wtime: 578720,
//   btime: 597720,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6 f1c4 d7d5 c4d5 c6d5 f3e5 d5e4 g2g3 d8d2 e1f1',
//   wtime: 565650,
//   btime: 597720,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6 f1c4 d7d5 c4d5 c6d5 f3e5 d5e4 g2g3 d8d2 e1f1 e4e3',
//   wtime: 565650,
//   btime: 595360,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6 f1c4 d7d5 c4d5 c6d5 f3e5 d5e4 g2g3 d8d2 e1f1 e4e3 c2c3',
//   wtime: 556810,
//   btime: 595360,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6 f1c4 d7d5 c4d5 c6d5 f3e5 d5e4 g2g3 d8d2 e1f1 e4e3 c2c3 c8h3',
//   wtime: 556810,
//   btime: 587450,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6 f1c4 d7d5 c4d5 c6d5 f3e5 d5e4 g2g3 d8d2 e1f1 e4e3 c2c3 c8h3 f1g1',
//   wtime: 547270,
//   btime: 587450,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6 f1c4 d7d5 c4d5 c6d5 f3e5 d5e4 g2g3 d8d2 e1f1 e4e3 c2c3 c8h3 f1g1 h3g2',
//   wtime: 547270,
//   btime: 581970,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6 f1c4 d7d5 c4d5 c6d5 f3e5 d5e4 g2g3 d8d2 e1f1 e4e3 c2c3 c8h3 f1g1 h3g2 b2b3',
//   wtime: 543850,
//   btime: 581970,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'started'
// }
// { type: 'gameFinish', game: { id: 'Wmv3QOtD' } }
// {
//   type: 'gameState',
//   moves: 'e2e4 e7e5 g1f3 c7c6 f1c4 d7d5 c4d5 c6d5 f3e5 d5e4 g2g3 d8d2 e1f1 e4e3 c2c3 c8h3 f1g1 h3g2 b2b3 d2f2',
//   wtime: 543850,
//   btime: 574250,
//   winc: 3000,
//   binc: 3000,
//   wdraw: false,
//   bdraw: false,
//   status: 'mate',
//   winner: 'black'
// }
