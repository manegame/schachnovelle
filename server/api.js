const axios = require('axios')
const https = require('https')
const ndjson = require('ndjson')

module.exports = {

  /**
   * get the user's information
   * @param {*} token 
   */
  getUserInfo (token) {
    return new Promise((resolve, reject) => {
      axios.get('/api/account', {
        baseURL: 'https://lichess.org/',
        headers: { 'Authorization': 'Bearer ' + token.access_token }
      })
      .then((response) => {
        resolve(response)
      })
      .catch((error) => {
        reject(error)
      })
    })
  },

  /**
   * Create an event stream
   * @param {*} token 
   */
  createEventStream (token) {
    const options = {
      headers: {
        'Authorization': 'Bearer ' + token.access_token
      }
    }

    return new Promise((resolve, reject) => { 
      const req = https.get('https://lichess.org/api/stream/event', options)

      req
        .on('response', (stream) => {
          const { statusCode } = stream
          
          if (statusCode >= 200 && statusCode < 300) {
            resolve(stream)
          }
        })
        .on('error', (error) => {
          reject(error)
        })
    })
  },

  createGameStream (token, gameId) {
    const options = {
      headers: {
        'Authorization': 'Bearer ' + token.access_token
      }
    }

    return new Promise((resolve, reject) => {
      const req = https.get(`https://lichess.org/api/board/game/stream/${gameId}`, options)
  
      req
        .on('response', (stream) => {
          const { statusCode } = stream
  
          if (statusCode >= 200 && statusCode < 300) {
            resolve(stream)
          }
        })
        .on('error', (error) => {
          reject(error)
        })
    })
  },

  // Commence play after the user has been identified
  startPlay (req, res) {
    return new Promise(async (resolve, reject) => {
      try {
        const stream = await this.createEventStream(req.session.accessToken)
        stream
          .pipe(ndjson.parse())
          .on('data', (data) => {
            // Always write the data type to the stream
            process.stdout.write(data.type)

            // Hook into events from event stream!
            if (data.type === 'gameStart') {
              req.session.currentGame = data.game
              this.openGameStream(req)
            } else {
              console.log('!!!   unhandled data type   !!!')
              console.log(data)
              console.log('!!! end unhandled data type !!!')
            }
          })
      } catch (error) {
        reject(error)
      } finally {
        resolve({ status: 200 })
      }
    })
  },

  // Tryout to follow a game
  async openGameStream (req) {
    try {
      const stream = await this.createGameStream(req.session.accessToken, req.session.currentGame.id)

      stream
        .pipe(ndjson.parse())
        .on('data', (data) => {
          // Write data type to stdout
          process.stdout.write(data.type)

          if (data.type === 'gameFull') {
            req.session.currentGame.data = data
          } else if (data.type === 'gameState') {
            req.session.currentGame.data.gameState = data
          } else {
            console.log('UNUSED DATA TYPE', data.type)
          }
        })
    } catch (error) {
      console.error(error)
    }
  }


}