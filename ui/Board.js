const chalk = require('chalk')

const Printer = require('./Printer')
const Utils = require('./Utils')

const files = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
const ranks = [1, 2, 3, 4, 5, 6, 7, 8]

class Board {
  constructor (white, black, options) {
    this.white = white
    this.black = black
    this.options = options

    if (this.options.orientation === 'white') {
      this.files = files
      this.ranks = ranks.reverse()
    } else {
      this.files = files.reverse()
      this.ranks = ranks
    }

    this.printer = new Printer(options.spacing)
    this.utils = new Utils()
  }

  clear () {
    this.printer.clearView()
  }

  render () {
    // this.topCheck()
    this.topRank()
    this.board()
    this.bottomRank()
    // this.bottomCheck()
  }

  getPiece (file, rank) {
    const activeWhitePiece = this.white.pieces.find((piece) => {
      return piece.pos[0] === file && piece.pos[1] === rank && !piece.captured
    })
    const activeBlackPiece = this.black.pieces.find((piece) => {
      return piece.pos[0] === file && piece.pos[1] === rank && !piece.captured
    })
    if (this.options.mode === 'color') {
      if (activeWhitePiece) {
        return chalk` {whiteBright ${activeWhitePiece.piece}} `
      } else if (activeBlackPiece) {
        return chalk` {black ${activeBlackPiece.piece}} `
      }
      return `   `
    } else if (this.options.mode === 'bare') {
      /**
       * Pieces
       */
      if (activeWhitePiece) {
        if (activeWhitePiece.type === 'King' && this.white.inCheck) {
          return chalk`{red ${activeWhitePiece.piece}}  `
        } else {
          return chalk`{black ${activeWhitePiece.piece}}  `
        }
      } else if (activeBlackPiece) {
        if (activeBlackPiece.type === 'King' && this.black.inCheck) {
          return chalk`{red ${activeBlackPiece.piece}}  `
        } else {
          return chalk`{black ${activeBlackPiece.piece}}  `
        }
      }
      /**
       * Empty squares
       */
      // If it's an odd rank it will start with black, so if you have an odd index in that rank, it will be black
      const rankParity = this.ranks.indexOf(rank) % 2 === 0 // the rank is odd
      const fileParity = this.files.indexOf(file) % 2 === 0 // the file is odd
      if (rankParity && fileParity || !rankParity && !fileParity) {
        return chalk`{white .}  `
      } else {
        return chalk`{black .}  `
      }
    }
  }

  /**
   * Returns all captured pieces from a color
   */
  capturedPieces (color) {
    return this[color].pieces.filter((piece) => { return piece.captured })
  }

  /**
   * Print the captured pieces for side
   */
  scoreBoard (side) {
    let scoreBoard = ''
    let score = 0
    const opponent = this.utils.opposingColor(side)
    const pieces = this.capturedPieces(opponent)
    
    if (pieces.length > 0) {
      pieces.forEach((piece, index) => {
        scoreBoard += this.printer.addSpace('x', piece.piece)
        score += piece.value
        if (index === pieces.length - 1) {
          scoreBoard += this.printer.addSpace('x', `(${score})`)
        }
      })

      this.printer.print(scoreBoard)
    }

    this.printer.addSpace('y')
  }

  /**
   * For development only. Display if the king is in check
   */
  topCheck () {
    const side = this.utils.opposingColor(this.options.orientation)
    this.printer.print(side + ' king in check: ' + this[side].inCheck)
    this.printer.addSpace('y')
  }

  /**
   * For development only. Display if the king is in check
   */
  bottomCheck () {
    const side = this.options.orientation
    this.printer.print(side + ' king in check: ' + this[side].inCheck)
    this.printer.addSpace('y')
  }

  /**
   * Top signage
   */
  topRank () {
    const side = this.utils.opposingColor(this.options.orientation)
    this.scoreBoard(side)
    let renderTopRank = this.printer.tab
    renderTopRank = this.printer.addSpace('x', renderTopRank)
    for (let file in this.files) {
      renderTopRank += `${this.files[file]}  `
      renderTopRank = this.printer.addSpace('x', renderTopRank)
    }
    this.printer.print(renderTopRank)
    this.printer.addSpace('y')
  }

  /**
   * Bottom signage
   */
  bottomRank () {
    let renderTopRank = this.printer.tab
    renderTopRank = this.printer.addSpace('x', renderTopRank)
    for (let file in this.files) {
      renderTopRank += `${this.files[file]}  `
      renderTopRank = this.printer.addSpace('x', renderTopRank)
    }
    this.printer.print(renderTopRank)
    this.printer.addSpace('y')
    this.scoreBoard(this.options.orientation)
  }

  board () {
    /**
     * Board
     */
    if (this.options.mode === 'color') {
      for (const rank in this.ranks) {
        let renderRank = `${this.ranks[rank]}  `
        renderRank = this.printer.addSpace('x', renderRank)

        for (const file in this.files) {
          // if rank is even, start with white
          if (rank % 2 === 0) {
            if (file % 2 === 0) {
              renderRank += chalk.bgKeyword('darkgrey')(this.getPiece(this.files[file], this.ranks[rank]))
            } else {
              renderRank += chalk.bgBlackBright(this.getPiece(this.files[file], this.ranks[rank]))
            }
            renderRank = this.printer.addSpace('x', renderRank)
          } else {
            if (file % 2 === 0) {
              renderRank += chalk.bgBlackBright(this.getPiece(this.files[file], this.ranks[rank]))
            } else {
              renderRank += chalk.bgKeyword('darkgrey')(this.getPiece(this.files[file], this.ranks[rank]))
            }
            renderRank = this.printer.addSpace('x', renderRank)
          }
        }
        renderRank += ` ${this.ranks[rank]}`
        this.printer.print(renderRank)
        this.printer.addSpace('y')
      }
    } else if (this.options.mode === 'bare') {
      for (const rank in this.ranks) {
        let renderRank = `${this.ranks[rank]}  `
        renderRank = this.printer.addSpace('x', renderRank)
        for (const file in this.files) {
          renderRank += this.getPiece(this.files[file], this.ranks[rank])
          renderRank = this.printer.addSpace('x', renderRank)
        }
        renderRank += ` ${this.ranks[rank]}`
        this.printer.printLine(renderRank)
        this.printer.addSpace('y')
      }
    }
  }
}

module.exports = Board