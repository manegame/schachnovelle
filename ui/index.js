const spacing = 1
const { Select } = require('enquirer')

const { spawn } = require('child_process')

const Game = require('./Game')
const Printer = require('./Printer')

const printer = new Printer(spacing)

/**
 * Welcome to Schachnovelle
 */
exports.welcomeMessage = () => {
  printer.addSpace('y')
  printer.printLine(printer.addSpaceBefore('S      .     C      .       V       .       E'))
  printer.printLine(printer.addSpaceBefore('   C      A     H       O       E       L    '))
  printer.printLine(printer.addSpaceBefore('.      H     .      N       .       L       .'))
  printer.addSpace('y')
  printer.printLine(printer.addSpaceBefore('      >>Um Gottes Willen! Nicht!<<'))
  printer.addSpace('y')
  printer.printLine(printer.addSpaceBefore('       Public Service Announcement           '))
  printer.printLine(printer.addSpaceBefore('      I run best on a white terminal          '))
  printer.addSpace('y')
}

// 
// 
// Spawn a server
exports.spawnServer = () => {
  const server = spawn("node", ["./server/index.js"]);

  server.stdout.on('data', (data) => {
    const responseCode = data.toString()

    if (responseCode === 'streamReady') {
      printer.clearView()

      this.welcomeMessage()
      printer.addSpace('y')
      printer.printLine(printer.addSpaceBefore('               Welcome'))
      printer.addSpace('y')

      setTimeout(() => {
        this.localGame()
      })
    }
  });
  
  server.stderr.on('data', (data) => {
    console.error(data.toString())
  });
  
  server.on('close', (code) => {
    // printer.clearView()
  });
}

// 
// 
// Just play a local game
exports.localGame = () => {
  let choices = {}

  const color = new Select({
    name: 'color',
    message: 'Play as',
    choices: ['white', 'black']
  })
  
  color.run()
    .then(answer => {
      choices.color = answer

      const game = new Game({
        playingAs: choices.color,
        spacing,
        mode: 'bare'
      })
          
      printer.clearView()

      game.init()
    })
    .catch(console.error)
}

// 
// 
// INIT
exports.init = () => {  
  // 
  // 
  // Don't be shy, say hello to that person!
  this.welcomeMessage()

  this.localGame()

  // const what = new Select({
  //   name: 'todo',
  //   message: 'How do you want to play?',
  //   choices: ['local', 'on lichess (beta)']
  // })

  // what.run()
  //   .then((answer) => {
  //     printer.addSpace('y')

  //     if (answer === 'local') {
  //     } else {
  //       this.spawnServer()

  //       printer.addSpace('y')
  //       printer.printLine(printer.addSpaceBefore('      Log in with Lichess to begin'))
  //       printer.addSpace('y')
  //     }
  //   })
}
