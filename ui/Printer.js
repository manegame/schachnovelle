const readline = require('readline')

class Printer {
  constructor (spacing) {
    this.spacing = spacing
    this.tab = '   '
    this.lineCount = 0
  }

  clearView () {
    const blank = '\n'.repeat(process.stdout.rows)
    console.log(blank)
    readline.cursorTo(process.stdout, 0, 0)
    readline.clearScreenDown(process.stdout)
    // while (this.lineCount > 0) {
    //   process.stdout.clearLine(0)
    //   this.lineCount--
    //   process.stdout.cursorTo(0, this.lineCount)
    // }
  }

  clearForward () {
    // process.stdout.clearScreenDown()
  }

  printLine (value) {
    process.stdout.write(value + '\n')
    this.lineCount++
  }

  print (value) {
    process.stdout.write(value)
  }

  addSpace (direction, string) {
    if (direction === 'x') {
      for (let i = 0; i < this.spacing; i++) {
        string += ' '
      }
    } else {
      for (let i = 0; i < this.spacing; i++) {
        process.stdout.write('\n')
        this.lineCount++
      }
    }
    return string
  }

  addSpaceBefore (string) {
    for (let i = 0; i < this.spacing; i++) {
      string = ' ' + string
    }
    return string
  }
}

module.exports =  Printer