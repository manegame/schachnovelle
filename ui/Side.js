class Side {
  constructor (color) {
    this.pieces = []
    if (color === 'white') {
      // 
      // 
      // 
      this.inCheck = false,
      //
      //
      //
      this.captured = []
      //
      //
      //
      this.pieces = [
        {
          pos: ['a', 2],
          prefix: 'p',
          type: 'Pawn',
          piece: '♙',
          value: 1
        },
        {
          pos: ['b', 2],
          prefix: 'p',
          type: 'Pawn',
          piece: '♙',
          value: 1
        },
        {
          pos: ['c', 2],
          prefix: 'p',
          type: 'Pawn',
          piece: '♙',
          value: 1
        },
        {
          pos: ['d', 2],
          prefix: 'p',
          type: 'Pawn',
          piece: '♙',
          value: 1
        },
        {
          pos: ['e', 2],
          prefix: 'p',
          type: 'Pawn',
          piece: '♙',
          value: 1
        },
        {
          pos: ['f', 2],
          prefix: 'p',
          type: 'Pawn',
          piece: '♙',
          value: 1
        },
        {
          pos: ['g', 2],
          prefix: 'p',
          type: 'Pawn',
          piece: '♙',
          value: 1
        },
        {
          pos: ['h', 2],
          prefix: 'p',
          type: 'Pawn',
          piece: '♙',
          value: 1
        },
        {
          pos: ['a', 1],
          prefix: 'R',
          type: 'Rook',
          piece: '♖',
          value: 5
        },
        {
          pos: ['h', 1],
          prefix: 'R',
          type: 'Rook',
          piece: '♖',
          value: 5
        },
        {
          pos: ['b', 1],
          prefix: 'N',
          type: 'Knight',
          piece: '♘',
          value: 3
        },
        {
          pos: ['g', 1],
          prefix: 'N',
          type: 'Knight',
          piece: '♘',
          value: 3
        },
        {
          pos: ['c', 1],
          prefix: 'B',
          type: 'Bishop',
          piece: '♗',
          value: 3
        },
        {
          pos: ['f', 1],
          prefix: 'B',
          type: 'Bishop',
          piece: '♗',
          value: 3
        },
        {
          pos: ['d', 1],
          prefix: 'Q',
          type: 'Queen',
          piece: '♕',
          value: 7
        },
        {
          pos: ['e', 1],
          prefix: 'K',
          type: 'King',
          piece: '♔',
          value: Infinity
        }
      ],
      //
      //
      //
      this.promotionChoices = [
        {
          pos: null,
          prefix: 'Q',
          type: 'Queen',
          piece: '♕',
          value: 7
        },
        {
          pos: null,
          prefix: 'R',
          type: 'Rook',
          piece: '♖',
          value: 5
        },
        {
          pos: null,
          prefix: 'B',
          type: 'Bishop',
          piece: '♗',
          value: 3
        },
        {
          pos: null,
          prefix: 'N',
          type: 'Knight',
          piece: '♘',
          value: 3
        }
      ]
    } else if (color === 'black') {
      // 
      // 
      // 
      this.inCheck = false,
      //
      //
      //
      this.captured = []
      //
      //
      //
      this.pieces = [
        {
          pos: ['a', 7],
          // pos: ['f', 3],
          prefix: 'p',
          type: 'Pawn',
          piece: '♟',
          value: 1
        },
        {
          pos: ['b', 7],
          prefix: 'p',
          type: 'Pawn',
          piece: '♟',
          value: 1
        },
        {
          pos: ['c', 7],
          prefix: 'p',
          type: 'Pawn',
          piece: '♟',
          value: 1
        },
        {
          pos: ['d', 7],
          prefix: 'p',
          type: 'Pawn',
          piece: '♟',
          value: 1
        },
        {
          pos: ['e', 7],
          prefix: 'p',
          type: 'Pawn',
          piece: '♟',
          value: 1
        },
        {
          pos: ['f', 7],
          prefix: 'p',
          type: 'Pawn',
          piece: '♟',
          value: 1
        },
        {
          pos: ['g', 7],
          prefix: 'p',
          type: 'Pawn',
          piece: '♟',
          value: 1
        },
        {
          pos: ['h', 7],
          prefix: 'p',
          type: 'Pawn',
          piece: '♟',
          value: 1
        },
        {
          pos: ['a', 8],
          prefix: 'R',
          type: 'Rook',
          piece: '♜',
          value: 5
        },
        {
          pos: ['h', 8],
          prefix: 'R',
          type: 'Rook',
          piece: '♜',
          value: 5
        },
        {
          pos: ['b', 8],
          prefix: 'N',
          type: 'Knight',
          piece: '♞',
          value: 3
        },
        {
          pos: ['g', 8],
          prefix: 'N',
          type: 'Knight',
          piece: '♞',
          value: 3
        },
        {
          pos: ['c', 8],
          prefix: 'B',
          type: 'Bishop',
          piece: '♝',
          value: 3
        },
        {
          pos: ['f', 8],
          prefix: 'B',
          type: 'Bishop',
          piece: '♝',
          value: 3
        },
        {
          pos: ['d', 8],
          prefix: 'Q',
          type: 'Queen',
          piece: '♛',
          value: 7
        },
        {
          pos: ['e', 8],
          prefix: 'K',
          type: 'King',
          piece: '♚',
          value: Infinity
        }
      ],
      //
      //
      //
      this.promotionChoices = [
        {
          pos: null,
          prefix: 'Q',
          type: 'Queen',
          piece: '♛',
          value: 7
        },
        {
          pos: null,
          prefix: 'R',
          type: 'Rook',
          piece: '♜',
          value: 5
        },
        {
          pos: null,
          prefix: 'B',
          type: 'Bishop',
          piece: '♝',
          value: 3
        },
        {
          pos: null,
          prefix: 'N',
          type: 'Knight',
          piece: '♞',
          value: 3
        }
      ]
    }
  }

  getPieces () {
    return this.pieces
  }
}

module.exports = Side