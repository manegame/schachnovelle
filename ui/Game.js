const readline = require("readline")
const Side = require("./Side")
const Board = require("./Board")
const Printer = require("./Printer")
const Utils = require("./Utils")

const Enquirer = require("enquirer")

class Game {
  constructor(options) {
    this.white = new Side("white")
    this.black = new Side("black")
    this.playingAs = options.playingAs
    this.toMove = "white"

    this.moveIndex = 0
    this.history = []
    this.selectedPiece = null
    this.selectedFile = null

    // Initialize the board
    this.board = new Board(this.white, this.black, {
      mode: "pieces", // possibly add text mode
      orientation: this.playingAs,
      spacing: options.spacing,
      mode: options.mode,
    })

    this.enquirer = new Enquirer()
    this.printer = new Printer(options.spacing)
    this.utils = new Utils()
  }

  /**
   * 1. Asks for the piece
   * 2. Sets that as the selected piece
   */
  async promptPieceSelection() {
    const response = await this.enquirer
      .prompt([
        {
          type: "select",
          name: "piece",
          message: "Piece",
          choices: this.listAvailablePieces(),
          result: result => {
            this.setSelectedPiece(result)
            return result
          },
        },
      ])
      .catch(console.error)

    /**
     * Only do something if the await has been fulfilled
     */
    if (response) {
      this.promptFileSelection()
    }
  }

  /**
   * Part two of the turn, making the actual move
   * 3. Asks for the new coordinates of that piece
   */
  async promptFileSelection() {
    const backToPiece = "... piece selection"
    const response = await this.enquirer
      .prompt([
        {
          type: "select",
          name: "file",
          message: "File:",
          choices: this.possibleFiles().concat(backToPiece),
          result: result => {
            if (result === backToPiece) {
              return result
            } else {
              this.setSelectedFile(result)
              return result
            }
          },
        },
      ])
      .catch(console.error)

    /**
     * Only do something if the await has been fulfilled
     */
    if (response) {
      if (response.file === backToPiece) {
        this.promptPieceSelection()
      } else {
        this.promptRankSelection()
      }
    }
  }

  /**
   * Part two of the turn, making the actual move
   * 3. Asks for the new coordinates of that piece
   */
  async promptRankSelection() {
    const backToFile = "... file selection"
    const response = await this.enquirer
      .prompt([
        {
          type: "select",
          name: "rank",
          message: "Rank:",
          choices: this.possibleRanks().concat(backToFile),
          result: result => {
            if (result === backToFile) {
              return result
            } else {
              this.setSelectedRank(Number(result))
              return Number(result)
            }
          },
        },
      ])
      .catch(console.error)

    /**
     * Only do something if the await has been fulfilled
     */
    if (response) {
      if (response.rank === backToFile) {
        this.promptFileSelection()
      } else {
        this.validateMove()
      }
    }
  }

  /**
   * For the given color, return all possible moves
   */
  allPossibleMoves(color) {
    let all = []
    this.board[color].pieces
      .filter(p => !p.captured)
      .forEach(p => {
        this.possibleMoves(p, color)
          .forEach(m => all.push(m))
      })
    return all
  }

  /**
   * Return all possible checks
   * @param {*} color
   * @returns
   */
  allPossibleChecks(color) {
    // Moves
    const moves = this.allPossibleMoves(color)

    const allChecks = moves.filter(m => m[2] === 'check')

    return allChecks
  }

  /**
   * Return the possible moves for the selected piece
   * Returns an object with all possible moves split into file, and rank
   */
  possibleMoves(piece, color) {
    // TODO: If the current color is in check, 
    // then limit the possible moves to the ones that get you out of it

    // Start with the selected piece. First of all, it can't move to it's own square
    switch (piece.type) {
      case "Pawn":
        return this.possiblePawnMoves(piece, color)
      case "Knight":
        return this.possibleKnightMoves(piece, color)
      case "Bishop":
        return this.possibleBishopMoves(piece, color)
      case "Rook":
        return this.possibleRookMoves(piece, color)
      case "Queen":
        return this.possibleQueenMoves(piece, color)
      case "King":
        return this.possibleKingMoves(piece, color)
    }
  }

  /**
   * Returns all possible moves for a pawn
   */
  possiblePawnMoves (pawn, color) {
    const possible = []
    const positions = []
    const direction = this.playingAs === color ? 1 : -1

    const nextRank = pawn.pos[1] + direction
    const secondNextRank = pawn.pos[1] + (direction * 2)
    // If it's on the starting rank, allow two moves
    if (pawn.pos[1] === 2 && color === "white") {
      positions.push([pawn.pos[0], nextRank], [pawn.pos[0], secondNextRank])
    } else if (pawn.pos[1] === 7 && color === "black") {
      positions.push([pawn.pos[0], nextRank], [pawn.pos[0], secondNextRank])
    } else if (color === "white") {
      positions.push([pawn.pos[0], nextRank])
    } else {
      positions.push([pawn.pos[0], nextRank])
    }
    // Captures
    const fileIndex = this.board.files.indexOf(pawn.pos[0])
    const positionsToCapture = []
    switch (fileIndex) {
      case 0:
        const posXL = [this.board.files[fileIndex + 1], nextRank]
        positionsToCapture.push(posXL)
        break
      case 7:
        const posXR = [this.board.files[fileIndex - 1], nextRank]
        positionsToCapture.push(posXR)
        break
      default:
        const posL = [this.board.files[fileIndex + 1], nextRank]
        const posR = [this.board.files[fileIndex - 1], nextRank]
        positionsToCapture.push(posL, posR)
        break
    }
    for (const pos of positions) {
      if (this.movePotential(pos, color, false) !== "blocked") {
        possible.push(pos)
      }
    }
    for (const pos of positionsToCapture) {
      if (this.movePotential(pos, color, true) === "capture") {
        possible.push(pos)
      } else if (this.movePotential(pos, color, true) === "check") {
        pos.push('check')
        possible.push(pos)
      }
    }
    // TODO: en passant (depends on a history)
    return possible
  }

  /**
   * Just returns the candidate positions a knight could move to
   * @param {Integer} fileIndex
   * @param {Integer} rankIndex
   */
  knightCandidateMoves(fileIndex, rankIndex) {
    return [
      // Two up, one left
      [this.board.files[fileIndex - 1], this.board.ranks[rankIndex + 2]],
      // Two up, one right
      [this.board.files[fileIndex + 1], this.board.ranks[rankIndex + 2]],
      // Two right, one up
      [this.board.files[fileIndex + 2], this.board.ranks[rankIndex + 1]],
      // Two right, one down
      [this.board.files[fileIndex + 2], this.board.ranks[rankIndex - 1]],
      // Two down, one right
      [this.board.files[fileIndex + 1], this.board.ranks[rankIndex - 2]],
      // Two down, one left
      [this.board.files[fileIndex - 1], this.board.ranks[rankIndex - 2]],
      // Two left, one down
      [this.board.files[fileIndex - 2], this.board.ranks[rankIndex - 1]],
      // Two left, one up
      [this.board.files[fileIndex - 2], this.board.ranks[rankIndex + 1]],
    ]
  }

  /**
   * Returns the possible moves for a knight
   */
  possibleKnightMoves(knight, color) {
    const fileIndex = this.board.files.indexOf(knight.pos[0])
    const rankIndex = this.board.ranks.indexOf(knight.pos[1])
    // From the starting square, draw a plus in each direction and then go one diagonal from there
    // Take the starting file and go left and right
    const candidates = this.knightCandidateMoves(fileIndex, rankIndex)

    const possible = candidates.filter(move => {
      if (move[0] !== undefined && move[1] !== undefined) {
        if (this.movePotential(move, color, true) === "check") {
          move.push('check')
          return move
        } else if (this.movePotential(move, color, true) !== "blocked") {
          return move
        }
      }
    })
    return possible
  }

  /**
   * Returns the possible moves for a bishop
   */
  possibleBishopMoves(bishop, color) {
    // go into all diagonal directions until there's no place or there's a capture or there's a block
    const fileIndex = this.board.files.indexOf(bishop.pos[0])
    const rankIndex = this.board.ranks.indexOf(bishop.pos[1])

    const fU = this.forwardAndUp(fileIndex, rankIndex, color)
    const fD = this.forwardAndDown(fileIndex, rankIndex, color, false)
    const bU = this.backwardAndUp(fileIndex, rankIndex, color, false)
    const bD = this.backwardAndDown(fileIndex, rankIndex, color, false)

    return [...fU, ...fD, ...bU, ...bD]
  }

  /**
   * Returns the possible moves for a rook
   */
  possibleRookMoves(rook, color) {
    // go into all diagonal directions until there's no place or there's a capture or there's a block
    const fileIndex = this.board.files.indexOf(rook.pos[0])
    const rankIndex = this.board.ranks.indexOf(rook.pos[1])

    const u = this.up(fileIndex, rankIndex, color, false)
    const d = this.down(fileIndex, rankIndex, color, false)
    const l = this.left(fileIndex, rankIndex, color, false)
    const r = this.right(fileIndex, rankIndex, color, false)

    return [...u, ...d, ...l, ...r]
  }

  /**
   * Returns the possible moves for a queen
   */
  possibleQueenMoves(queen, color) {
    // go into all diagonal directions until there's no place or there's a capture or there's a block
    const fileIndex = this.board.files.indexOf(queen.pos[0])
    const rankIndex = this.board.ranks.indexOf(queen.pos[1])

    const u = this.up(fileIndex, rankIndex, color, false)
    const d = this.down(fileIndex, rankIndex, color, false)
    const l = this.left(fileIndex, rankIndex, color, false)
    const r = this.right(fileIndex, rankIndex, color, false)
    const fU = this.forwardAndUp(fileIndex, rankIndex, color, false)
    const fD = this.forwardAndDown(fileIndex, rankIndex, color, false)
    const bU = this.backwardAndUp(fileIndex, rankIndex, color, false)
    const bD = this.backwardAndDown(fileIndex, rankIndex, color, false)

    return [...u, ...d, ...l, ...r, ...fU, ...fD, ...bU, ...bD]
  }

  /**
   * Returns the possible moves for a queen
   */
  possibleKingMoves(king, color) {
    // go into all diagonal directions until there's no place or there's a capture or there's a block
    const fileIndex = this.board.files.indexOf(king.pos[0])
    const rankIndex = this.board.ranks.indexOf(king.pos[1])

    const u = this.up(fileIndex, rankIndex, color, true)
    const d = this.down(fileIndex, rankIndex, color, true)
    const l = this.left(fileIndex, rankIndex, color, true)
    const r = this.right(fileIndex, rankIndex, color, true)
    const fU = this.forwardAndUp(fileIndex, rankIndex, color, true)
    const fD = this.forwardAndDown(fileIndex, rankIndex, color, true)
    const bU = this.backwardAndUp(fileIndex, rankIndex, color, true)
    const bD = this.backwardAndDown(fileIndex, rankIndex, color, true)

    return [...u, ...d, ...l, ...r, ...fU, ...fD, ...bU, ...bD]
  }

  /**
   * Returns the possible moves on a diagonal going right and up
   * This function is used in two contexts;
   * 1. To determine the next moves for a person playing
   * 2. To check if the next move will be checking the opponent's king
   */
  forwardAndUp(fileIndex, rankIndex, color, king) {
    const possible = []
    let fi = fileIndex + 1
    let ri = rankIndex + 1

    let nextPosition = [this.board.files[fi], this.board.ranks[ri]]

    while (
      this.movePotential(nextPosition, color, true) !== "blocked" &&
      this.movePotential(nextPosition, color, true) !== "impossible"
    ) {
      if (this.movePotential(nextPosition, color, true) === "check") {
        nextPosition.push('check')
        possible.push(nextPosition)
        break
      }
      possible.push(nextPosition)
      if (this.movePotential(nextPosition, color, true) === "capture" || king) break
      fi++
      ri++
      nextPosition = [this.board.files[fi], this.board.ranks[ri]]
    }

    return possible
  }

  /**
   * Returns the possible moves on a diagonal going right and down
   */
  forwardAndDown(fileIndex, rankIndex, color, king) {
    const possible = []
    let fi = fileIndex + 1
    let ri = rankIndex + -1

    let nextPosition = [this.board.files[fi], this.board.ranks[ri]]

    while (
      this.movePotential(nextPosition, color, true) !== "blocked" &&
      this.movePotential(nextPosition, color, true) !== "impossible"
    ) {
      if (this.movePotential(nextPosition, color, true) === "check") {
        nextPosition.push('check')
        possible.push(nextPosition)
        break
      }
      possible.push(nextPosition)
      if (this.movePotential(nextPosition, color, true) === "capture" || king) break
      fi++
      ri--
      nextPosition = [this.board.files[fi], this.board.ranks[ri]]
    }
    return possible
  }

  /**
   * Returns the possible moves on a diagonal going left and up
   */
  backwardAndUp(fileIndex, rankIndex, color, king) {
    const possible = []
    let fi = fileIndex - 1
    let ri = rankIndex + 1

    let nextPosition = [this.board.files[fi], this.board.ranks[ri]]

    while (
      this.movePotential(nextPosition, color, true) !== "blocked" &&
      this.movePotential(nextPosition, color, true) !== "impossible"
    ) {
      if (this.movePotential(nextPosition, color, true) === "check") {
        nextPosition.push('check')
        possible.push(nextPosition)
        break
      }
      possible.push(nextPosition)
      if (this.movePotential(nextPosition, color, true) === "capture" || king) break
      fi--
      ri++
      nextPosition = [this.board.files[fi], this.board.ranks[ri]]
    }
    return possible
  }

  /**
   * Returns the possible moves on a diagonal going left and down
   */
  backwardAndDown(fileIndex, rankIndex, color, king) {
    const possible = []
    let fi = fileIndex - 1
    let ri = rankIndex - 1

    let nextPosition = [this.board.files[fi], this.board.ranks[ri]]

    while (
      this.movePotential(nextPosition, color, true) !== "blocked" &&
      this.movePotential(nextPosition, color, true) !== "impossible"
    ) {
      if (this.movePotential(nextPosition, color, true) === "check") {
        nextPosition.push('check')
        possible.push(nextPosition)
        break
      }
      possible.push(nextPosition)
      if (this.movePotential(nextPosition, color, true) === "capture" || king) break
      fi--
      ri--
      nextPosition = [this.board.files[fi], this.board.ranks[ri]]
    }
    return possible
  }

  /**
   * Returns the possible moves on a file going up
   */
  up(fileIndex, rankIndex, color, king) {
    const possible = []
    let ri = rankIndex + 1

    let nextPosition = [this.board.files[fileIndex], this.board.ranks[ri]]

    while (
      this.movePotential(nextPosition, color, true) !== "blocked" &&
      this.movePotential(nextPosition, color, true) !== "impossible"
    ) {
      if (this.movePotential(nextPosition, color, true) === "check") {
        nextPosition.push('check')
        possible.push(nextPosition)
        break
      }
      possible.push(nextPosition)
      if (this.movePotential(nextPosition, color, true) === "capture" || king) break
      ri++
      nextPosition = [this.board.files[fileIndex], this.board.ranks[ri]]
    }
    return possible
  }

  /**
   * Returns the possible moves on a file going down
   */
  down(fileIndex, rankIndex, color, king) {
    const possible = []
    let ri = rankIndex - 1

    let nextPosition = [this.board.files[fileIndex], this.board.ranks[ri]]

    while (
      this.movePotential(nextPosition, color, true) !== "blocked" &&
      this.movePotential(nextPosition, color, true) !== "impossible"
    ) {
      if (this.movePotential(nextPosition, color, true) === "check") {
        nextPosition.push('check')
        possible.push(nextPosition)
        break
      }
      possible.push(nextPosition)
      if (this.movePotential(nextPosition, color, true) === "capture" || king) break
      ri--
      nextPosition = [this.board.files[fileIndex], this.board.ranks[ri]]
    }
    return possible
  }

  /**
   * Returns the possible moves on a rank going left
   */
  left(fileIndex, rankIndex, color, king) {
    const possible = []
    let fi = fileIndex - 1

    let nextPosition = [this.board.files[fi], this.board.ranks[rankIndex]]

    while (
      this.movePotential(nextPosition, color, true) !== "blocked" &&
      this.movePotential(nextPosition, color, true) !== "impossible"
    ) {
      if (this.movePotential(nextPosition, color, true) === "check") {
        nextPosition.push('check')
        possible.push(nextPosition)
        break
      }
      possible.push(nextPosition)
      if (this.movePotential(nextPosition, color, true) === "capture" || king) break
      fi--
      nextPosition = [this.board.files[fi], this.board.ranks[rankIndex]]
    }
    return possible
  }

  /**
   * Returns the possible moves on a rank going right
   */
  right(fileIndex, rankIndex, color, king) {
    const possible = []
    let fi = fileIndex + 1

    let nextPosition = [this.board.files[fi], this.board.ranks[rankIndex]]

    while (
      this.movePotential(nextPosition, color, true) !== "blocked" &&
      this.movePotential(nextPosition, color, true) !== "impossible"
    ) {
      if (this.movePotential(nextPosition, color, true) === "check") {
        nextPosition.push('check')
        possible.push(nextPosition)
        break
      }
      possible.push(nextPosition)
      if (this.movePotential(nextPosition, color, true) === "capture" || king) break
      fi++
      nextPosition = [this.board.files[fi], this.board.ranks[rankIndex]]
    }
    return possible
  }

  /**
   * Returns the possible ranks from the possiblemoves function
   */
  possibleFiles() {
    const possible = this.possibleMoves(this.selectedPiece, this.toMove).map(pos => {
      return pos[0]
    })
    /** Filter unique */
    return [...new Set(possible)]
  }

  /**
   * Returns the possible ranks from the possiblemoves function
   */
  possibleRanks() {
    const possible = this.possibleMoves(this.selectedPiece, this.toMove)
      .filter(move => {
        if (move[0] === this.selectedFile) {
          return move[1]
        }
      })
      .map(pos => {
        return pos[1].toString()
      })
    /** Filter unique */
    return [...new Set(possible)]
  }

  /**
   * 1. Process captures
   * 2. TODO: Check check
   * 3. TODO: Check checkmate
   */
  async validateMove() {
    // Commit the move to update the board
    this.processCapture()
    await this.promptPromotion()
    this.commitMove()
  }

  /**
   * Allowed configures the check to consider if a capture may be made
   * If it is true,
   * Returns 'capture' if there is an opposing piece at the given position
   * Returns 'check' if the opposing piece at the given position is a king
   * Returns 'blocked' if there is an own piece or 'possible' for no piece at the given position
   */
  movePotential(pos, color, allowed) {
    if (pos[0] === undefined || pos[1] === undefined) {
      return "impossible"
    }

    const opponentsPieces =
      this.board[this.utils.opposingColor(color)].pieces

    const ownPiece = this.board[color].pieces.find(piece => {
      return piece.pos[0] === pos[0] && piece.pos[1] === pos[1]
    })
    const opponentsPiece = opponentsPieces.find(piece => {
      return piece.pos[0] === pos[0] && piece.pos[1] === pos[1]
    })

    if (opponentsPiece && allowed) {
      if (opponentsPiece.type === "King") {
        return "check"
      } else {
        return "capture"
      }
    }
    if (ownPiece && allowed) {
      return "blocked"
    }
    if (ownPiece || (opponentsPiece && !allowed)) {
      return "blocked"
    }
    if (!ownPiece && !opponentsPiece) {
      return "possible"
    }
  }

  /**
   * Processes a capture.
   * 1. Checks for capture
   * 2. If capture, set the piece status to captured
   */
  processCapture() {
    const opponentsPieces =
      this.board[this.utils.opposingColor(this.toMove)].pieces
    const capturedPiece = opponentsPieces.find(piece => {
      return (
        piece.pos[0] === this.selectedFile && piece.pos[1] === this.selectedRank
      )
    })
    if (capturedPiece) {
      capturedPiece.captured = true
    }
  }

  /**
   * Prompt promotion
   * 1. Asks user which piece to promote to
   * 2. Sets said piece to its new values
   */
  async promptPromotion() {
    if (
      (this.selectedPiece.type === "Pawn" &&
        this.toMove === "white" &&
        this.selectedRank === 8) ||
      (this.selectedPiece.type === "Pawn" &&
        this.toMove === "black" &&
        this.selectedRank === 1)
    ) {
      await this.enquirer
        .prompt({
          type: "select",
          name: "promotion",
          message: "Promote to:",
          choices: this.board[this.toMove].promotionChoices.map(choice => {
            return choice.piece
          }),
          result: piece => {
            const promotionChoice = this.board[
              this.toMove
            ].promotionChoices.find(choice => {
              return choice.piece === piece
            })
            const selectedPiece = this.getSelectedPiece()
            selectedPiece.piece = piece
            selectedPiece.type = promotionChoice.type
            selectedPiece.prefix = promotionChoice.prefix
            selectedPiece.value = promotionChoice.piece
            return selectedPiece
          },
        })
        .catch(console.error)
    }
  }

  /**
   * Check master function. Is run right before passTurn and board.render
   */
  checkForCheck() {
    // If the opponent's checks length is > 0, that means you're in check
    const opponent = this.utils.opposingColor(this.toMove)
    const opponentsChecks = this.allPossibleChecks(opponent)

    if (opponentsChecks.length > 0) {
      this.board[this.toMove].inCheck = true
    } else {
      this.board[this.toMove].inCheck = false
      this.board[opponent].inCheck = false
    }
  }

  /**
   * Passes the turn to the other player by setting the toMove var and prompting the next move
   */
  passTurn() {
    this.toMove = this.toMove === "white" ? "black" : "white"
    this.checkForCheck()
    this.promptPieceSelection()
  }

  /**
   * Commits the given move
   * 1. Sets the position of the selected piece
   * 2. Sets the move in the game's history
   * 3. Passes the turn to the other player
   * 4. Renders the board
   */
  commitMove() {
    this.getSelectedPiece().pos = [this.selectedFile, this.selectedRank]
    this.commitToHistory()
    this.passTurn()
    this.board.clear()
    this.board.render()
  }

  /**
   * Pushes move to history
   */
  commitToHistory() {}

  /**
   * Returns a list of available pieces for the prompt
   */
  listAvailablePieces() {
    return this.board[this.toMove].pieces
      .filter(piece => {
        return !piece.captured && this.possibleMoves(piece, this.toMove).length > 0
      })
      .map(piece => {
        return `${piece.piece} (${piece.pos})`
      })
  }

  /**
   * Sets the given piece as selected for this move
   */
  setSelectedPiece(piece) {
    const selection = this.board[this.toMove].pieces.find(p => {
      return p.pos[0] === piece[3] && p.pos[1] === Number(piece[5])
    })
    this.selectedPiece = selection
  }

  /**
   * Sets the given file as selected for this move
   */
  setSelectedFile(file) {
    this.selectedFile = file
  }

  /**
   * Sets the given rank as selected for this move
   */
  setSelectedRank(rank) {
    this.selectedRank = rank
  }

  /**
   * Get the current player's piece
   */
  getPiece(type) {
    return this.board[this.toMove].pieces.find(piece => {
      return piece.type === type
    })
  }

  /**
   * Get the opponent's piece
   */
  getOpponentsPiece(type) {
    return this.board[this.utils.opposingColor(this.toMove)].pieces.find(
      piece => {
        return piece.type === type
      }
    )
  }

  /**
   * returns the selected piece for this move
   */
  getSelectedPiece() {
    return this.board[this.toMove].pieces.find(piece => {
      return piece === this.selectedPiece
    })
  }

  /**
   * Um Gottes Wille, nicht!
   */
  init() {
    this.board.clear()
    this.board.render()
    this.promptPieceSelection()
  }
}

module.exports = Game
