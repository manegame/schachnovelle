export default {
  moves: [],

  makeMove: (move) => {
    this.moves.push(move)
  },

  getMoves: () => {
    return this.moves.join(' ')
  }
}
