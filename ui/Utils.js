class Utils {
  /**
   * Returns opposing party's color
   */
  opposingColor (color) {
    const opposing = color === 'white' ? 'black' : 'white'
    return opposing
  }

  /**
   * Gets piece at current position
   */
  getPieceAt (set, pos) {
    return set.find((piece) => {
      return piece.pos[0] === pos[0] && piece.pos[1] === pos[1]
    })
  }
}

module.exports = Utils